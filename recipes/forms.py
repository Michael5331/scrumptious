from django.forms import ModelForm
from recipes.models import Recipe
from django import forms


class RecipeForm(ModelForm):
    class Meta:
        model = Recipe
        fields = [
            "title",
            "picture",
            "description",
            "rating",

        ]
